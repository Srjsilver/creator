package org.stalem.creator.api;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DatabaseData {
    private String databaseUri;
    private String databaseUsername;
    private String databasePassword;
    private String databaseDriverClassName;
}
