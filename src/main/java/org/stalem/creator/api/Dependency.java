package org.stalem.creator.api;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Dependency {
    private String groupId;
    private String artifactId;
    private String version;
}
