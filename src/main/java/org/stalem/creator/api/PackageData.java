package org.stalem.creator.api;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class PackageData {
    private DatabaseData databaseData;
    private String group;
    private boolean flyway;
    private boolean springBoot;
    private SourceCompatibilityJavaVersion sourceCompatibilityJavaVersion;

    private List<Dependency> dependencyList;

}
