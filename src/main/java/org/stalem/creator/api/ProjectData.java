package org.stalem.creator.api;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ProjectData {
    private String projectName;
    private String projectDescription;
    private PackageData packageData;
    private ServiceData serviceData;
}
