package org.stalem.creator.api;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.springframework.lang.Nullable;

@Builder
@Data
public class ServiceData {

    @NonNull
    private String dataPrimaryType;
    @NonNull
    private String jsonString;
    @Nullable
    private String primaryKey;
    @Nullable
    private String primaryKeyType;
}
