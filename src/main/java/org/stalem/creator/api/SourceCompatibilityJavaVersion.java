package org.stalem.creator.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum SourceCompatibilityJavaVersion {
    Java7("7"),
    Java8("8"),
    Java9("9");

    @Getter private String value;
}

