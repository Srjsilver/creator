package org.stalem.creator.builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.component.*;
import org.stalem.creator.utils.FileHandler;
import org.stalem.creator.utils.JavaFileHelper;
import org.stalem.creator.utils.PackageHelper;
import org.stalem.creator.utils.PathHelper;

import java.util.Map;
import java.util.Set;


@Service
public class MicroserviceFactory {

    @Autowired
    private ProjectGradleComposer projectGradleComposer;

    @Autowired
    private DomainDataComposer domainDataComposer;

    @Autowired
    private ApplicationComposer applicationComposer;

    //@Autowired
    //private DockerFileComposer dockerFileComposer;

    @Autowired
    private ConfigFileComposer configFileComposer;

    @Autowired
    private ReadmeFileComposer readmeFileComposer;

    @Autowired
    private RepositoryFileComposer repositoryFileComposer;

    @Autowired
    private ServiceFileComposer serviceFileComposer;

    @Autowired
    private ControllerFileComposer controllerFileComposer;

    private final static String srcMainJava = "/src/main/java/";
    private final static String srcMain = "/src/main/";

    public String generateMicroservice(ProjectData projectData) {
        String root = projectGradleComposer.createProjectStructure(projectData);
        String rootOfGroupMain = root + srcMainJava + PackageHelper.generatePackageLocationWithSlashes(projectData.getPackageData());
        String rootSrcMainJava = root + srcMainJava;
        String rootSrcMain = root + srcMain;
        String[] applicationProperties = applicationComposer.generateApplicationProperties(projectData); //not done
        String[] applicationMain = applicationComposer.generateApplicationMain(projectData); //not done
        //String[] dockerFile = dockerFileComposer.generateDockerFile(rootOfGroupMain, projectData);
        //String[] readmeFile = readmeFileComposer.generateReadme(rootOfGroupMain, projectData); //incomplete
        //String[] configFile = configFileComposer.generateConfigFiles(rootOfGroupMain, projectData); //incomplete
        Map<String, String> domainJavafiles = domainDataComposer.generateMapOfPackageAndJavaClasses(projectData.getServiceData(), projectData.getPackageData());

        String[] repositoryFile = repositoryFileComposer.generateRepository(projectData);
        String[] serviceFile = serviceFileComposer.generateService(projectData);
        String[] controllerFile = controllerFileComposer.generateController(projectData);

        //create domain classes
        createJavaFiles(domainJavafiles, rootSrcMainJava);
        //create application.properties
        createFile(applicationProperties, rootSrcMain);
        //create application
        createApplicationMain(applicationMain, rootOfGroupMain);
        //create repository
        createFile(repositoryFile, rootOfGroupMain);
        //create service
        createFile(serviceFile, rootOfGroupMain);
        //create controller
        createFile(controllerFile, rootOfGroupMain);
        return root;
        //done
    }

    private void createFile(String[] genericPathContent, String rootSrcMain){
        String directory = FileHandler.removeFileFromPackage(genericPathContent[0]);
        FileHandler.createDirectories(rootSrcMain + directory);
        FileHandler.createFileAndInsertContent(rootSrcMain + genericPathContent[0], genericPathContent[1]);
    }
    private void createApplicationMain(String[] genericPathContent, String rootSrcMain){
        FileHandler.createDirectories(rootSrcMain);
        FileHandler.createFileAndInsertContent(rootSrcMain + genericPathContent[0], genericPathContent[1]);
    }

    private void createJavaFiles(Map<String,String> domainJavafiles, String root) {
        Set<Map.Entry<String, String>> entrySett = domainJavafiles.entrySet();
/*        for(Map.Entry entry : entrySett){
            JavaFileHelper.createJavaFile((String)entry.getValue(), (root + PathHelper.convertDotToSlashPath((String)entry.getKey())));
        }*/
        domainJavafiles.entrySet().forEach(entry -> JavaFileHelper.createJavaFile(entry.getValue(), (root + PathHelper.convertDotToSlashPath(entry.getKey()))));
    }

}
