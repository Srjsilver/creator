package org.stalem.creator.component;

import org.springframework.stereotype.Component;
import org.stalem.creator.api.ProjectData;

@Component
public class ApplicationComposer {

    public static String imports =
           // "import com.fasterxml.jackson.core.type.TypeReference;\n" +
           // "import com.fasterxml.jackson.databind.ObjectMapper;\n" +
           // "import org.springframework.boot.CommandLineRunner;\n" +
            "import org.springframework.boot.SpringApplication;\n" +
            "import org.springframework.boot.autoconfigure.SpringBootApplication;\n"
           // "import org.springframework.context.annotation.Bean;\n";
            +"\n";

    //fileName, content
    public String[] generateApplicationProperties(ProjectData projectData) {
        String fileName = "application.yml";
        String restOfPathToResources = "/resources/";
        String content = "";
        if(projectData.getPackageData().isSpringBoot()){
            content += "spring:\n";
        }
        if(!projectData.getPackageData().getDatabaseData().getDatabaseUri().isEmpty()){
            content += "   datasource:\n" +
                       "        url: " + projectData.getPackageData().getDatabaseData().getDatabaseUri() + "\n" +
                       "        username: " + projectData.getPackageData().getDatabaseData().getDatabaseUsername() + "\n" +
                       "        password: " + projectData.getPackageData().getDatabaseData().getDatabasePassword() + "\n" +
                       "        driver-class-name: " + projectData.getPackageData().getDatabaseData().getDatabaseDriverClassName() +"\n" +
                       "   jpa:\n" +
                       "        generate-ddl: true";

        }
        if(projectData.getPackageData().isFlyway()){
            content += "  flyway:\n" +
                       "      clean-on-validation-error: true\n";
        }
        return new String[]{restOfPathToResources + fileName, content};
    }

    public String[] generateApplicationMain(ProjectData projectData) {
        String groupPath = projectData.getPackageData().getGroup();
        String packageLine = "package " + groupPath + ";\n\n";
        String fileName = "/Application.java";
        //String importDomainSuperType = "import " + groupPath + "." + "domain." + projectData.getServiceData().getDataPrimaryType() +"\n";
        //String importServiceType = "import " + groupPath + "." + "service." + projectData.getServiceData().getDataPrimaryType() + "Service\n\n";
        String content = packageLine + imports;
                content +=
                "@SpringBootApplication\n" +
                        "public class Application {\n" +
                        "\n" +
                        "\tpublic static void main(String[] args) {\n" +
                        "\t\tSpringApplication.run(Application.class, args);\n" +
                        "\t}\n" +
                        "}";
        return new String[]{fileName, content};
    }
}
