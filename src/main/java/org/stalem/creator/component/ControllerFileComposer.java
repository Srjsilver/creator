package org.stalem.creator.component;

import org.springframework.stereotype.Component;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.utils.JavaTypeHelper;

import javax.websocket.server.PathParam;

@Component
public class ControllerFileComposer {

    private final String imports =
            "import org.springframework.web.bind.annotation.GetMapping;\n" +
                    "import org.springframework.web.bind.annotation.PathVariable;\n" +
                    "import org.springframework.web.bind.annotation.DeleteMapping;\n" +
                    "import org.springframework.web.bind.annotation.PostMapping;\n" +
                    "import org.springframework.web.bind.annotation.RequestBody;\n" +
                    "import org.springframework.web.bind.annotation.RequestMapping;\n" +
                    "import org.springframework.web.bind.annotation.RestController;\n";

    public String[] generateController(ProjectData projectData) {
        String primaryType = projectData.getServiceData().getDataPrimaryType();
        String fileName = primaryType + "Controller.java";
        String pathToController = "/controller/";
        String variableFirstLetterLowerCaseOfType = JavaTypeHelper.lowerCaseFirstLetterOfString(primaryType);
        String primaryKeyType = projectData.getServiceData().getPrimaryKeyType();
        String primaryKeyValue = projectData.getServiceData().getPrimaryKey();
        String packageLine = "package " + projectData.getPackageData().getGroup() + ".service;\n\n";
        String importLines = imports + "import "
                + projectData.getPackageData().getGroup() + ".domain."
                + primaryType + ";\n";
        importLines += "import " + projectData.getPackageData().getGroup() + ".service." +
                primaryType + "Service;\n\n";
        String content = packageLine + importLines;
        content +=
                "@RestController\n" +
                        "@RequestMapping(\"/" + variableFirstLetterLowerCaseOfType + "s" + "\")\n" +
                        "public class " + primaryType + "Controller {\n" +
                        "\n" +
                        "    private " + primaryType + "Service " + variableFirstLetterLowerCaseOfType + "Service;\n" +
                        "\n" +
                        "    public " + primaryType + "Controller(" + primaryType + "Service " + variableFirstLetterLowerCaseOfType + "Service) {\n" +
                        "        this." + variableFirstLetterLowerCaseOfType + "Service = " + variableFirstLetterLowerCaseOfType + "Service;\n" +
                        "    }\n" +
                        "\n" +
                        "    @GetMapping(\"/list\")\n" +
                        "    public Iterable<" + primaryType + "> list() {\n" +
                        "        return " + variableFirstLetterLowerCaseOfType + "Service.list();\n" +
                        "    }\n" +
                        "\n";
        //more endpoints
        content += generateAdd(primaryType, variableFirstLetterLowerCaseOfType);
        content += generateFindById(primaryKeyType, primaryKeyValue, primaryType, variableFirstLetterLowerCaseOfType);
        content += generateDeleteById(primaryKeyType, primaryKeyValue, variableFirstLetterLowerCaseOfType);
        content += "}";
        return new String[]{pathToController + fileName, content};
    }

    /**
     * CRUD
     * Add, Read, Update, Delete
     * In future make such that we can make custom queries
     */
    public String generateAdd(String type, String variableOfType) {
        String retVal = "    @PostMapping(\"/add/" + variableOfType + "\")\n";
        retVal += "    public " + type + " add" + type + "(@RequestBody " + type + " " + variableOfType + "){\n";
        retVal += "        return " + variableOfType + "Service.save(" + variableOfType + ");";
        retVal += "    }\n";
        return retVal;
    }
    public String generateFindById(String type, String variablOfType, String returnType, String returnVariableOfType){
        String retVal = "   @GetMapping(\"/findById/{" + variablOfType + "}\")\n";
        retVal += "     public " + returnType + " findById" + "(@PathVariable(\"" + variablOfType +"\") " + type + " " + variablOfType +"){\n";
        retVal += "        return " + returnVariableOfType + "Service.findById(" + variablOfType + ");";
        retVal += "    }\n";
        return retVal;
    }
    public String generateDeleteById(String type, String variableOfType, String retunVariableOfType){
        String retVal = "   @DeleteMapping(\"/deleteById/{" + variableOfType + "}\")\n";
        retVal += "     public " + "void" + " deleteById" + "(@PathVariable(\"" + variableOfType +"\") " + type + " " + variableOfType +"){\n";
        retVal += "         " + retunVariableOfType + "Service.deleteById(" + variableOfType + ");";
        retVal += "    }\n";
        return retVal;
    }
}
