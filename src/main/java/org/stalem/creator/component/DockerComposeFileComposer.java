package org.stalem.creator.component;

import org.springframework.stereotype.Component;
import org.stalem.creator.api.ProjectData;

@Component
public class DockerComposeFileComposer {


    public String generateDockerComposeFile(ProjectData packageData){
        String content =
                        "version: '3.1'\n" +
                        "\n" +
                        "services:\n" +
                        "  springboot:\n" +
                        "    build: .\n" +
                        "    # image: registry.gitlab.com/idgst/springboot-mongo-demo:latest\n" +
                        "    restart: always\n" +
                        "    container_name: springboot\n" +
                        "    ports:\n" +
                        "      - 8182:8080\n" +
                        "    working_dir: /opt/app\n" +
                        "    depends_on:\n" +
                        "      - mongo\n" +
                        "  \n" +
                        "  mongo:\n" +
                        "    image: mongo\n" +
                        "    container_name: springboot-mongo\n" +
                        "#    ports:  # for demo/debug purpose only\n" +
                        "#      - 27018:27017\n" +
                        "    volumes:\n" +
                        "      - $HOME/data/springboot-mongo-data:/data/db\n" +
                        "      - $HOME/data/springboot-mongo-bkp:/data/bkp\n" +
                        "    restart: always";
        return content;
    }

    public String generateDockerFile(ProjectData packageData){
        String content =
                "FROM openjdk:8-alpine\n" +
                        "\n" +
                        "# Required for starting application up.\n" +
                        "RUN apk update && apk add bash\n" +
                        "\n" +
                        "RUN mkdir -p /opt/app\n" +
                        "ENV PROJECT_HOME /opt/app\n" +
                        "\n" +
                        "COPY build/libs/springboot-mongo-demo.jar $PROJECT_HOME/springboot-mongo-demo.jar\n" +
                        "\n" +
                        "WORKDIR $PROJECT_HOME\n" +
                        "\n" +
                        "CMD [\"java\", \"-Dspring.data.mongodb.uri=mongodb://springboot-mongo:27017/springmongo-demo\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"./springboot-mongo-demo.jar\"]";
        return content;
    }
}
