package org.stalem.creator.component;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Component;
import org.stalem.creator.api.PackageData;
import org.stalem.creator.api.ServiceData;
import org.stalem.creator.utils.JavaTypeHelper;
import org.stalem.creator.utils.JsonNodeHelper;
import org.stalem.creator.utils.PackageHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component
public class DomainDataComposer {

    private static String imports_of_domain_type =
            "import com.fasterxml.jackson.annotation.JsonIgnore;\n" +
                    "import lombok.AllArgsConstructor;\n" +
                    "import lombok.NoArgsConstructor;\n" +
                    "import lombok.Data;\n" +
                    "\n" +
                    "import javax.persistence.*;\n\n";


    public Map<String, String> generateMapOfPackageAndJavaClasses(ServiceData serviceData, PackageData packageData) {
        String jsonString = serviceData.getJsonString();
        Map<String, String> retVal = new HashMap<>();
        try {
            JsonNode node = JsonNodeHelper.getNodeFromJsonString(jsonString);
            boolean topLevel = true;
            generateDomainClasses(node, serviceData.getDataPrimaryType(), retVal, packageData, serviceData, topLevel);
            return retVal;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    /*
    for testing purposes kept public
    OBS! need set<String> of variable names to check for unique column names
     */
    public void generateDomainClasses(
            JsonNode node,
            String currOwnerClazz,
            Map<String, String> packageClazzMap,
            PackageData packageData,
            ServiceData serviceData,
            boolean isTopLevel
    ) {
        Iterator<String> fieldNames = node.fieldNames();
        String javaClazzContent = "";
        String javaClazzContentPackage = PackageHelper.generatePackage(packageData, "domain;");
        String imports = null;
        javaClazzContent += getEntityLombokClassAnnotations(currOwnerClazz);
        javaClazzContent += getClazzName(JavaTypeHelper.upperCaseFirstLetterOfString(currOwnerClazz));
        if(!isTopLevel){
            javaClazzContent += generateId(currOwnerClazz.toLowerCase() + "_id", "long", true);
        }
        //HERE
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            JsonNode fieldValue = node.get(fieldName);
            if(serviceData.getPrimaryKey().equals(fieldName) && isTopLevel) {
                javaClazzContent += generateId(fieldName, serviceData.getPrimaryKeyType(), false);
            }else {
                if (fieldValue.isObject()) { //embedded
                    javaClazzContent += generateJoinTableClazz(fieldName);
                    generateDomainClasses(fieldValue, fieldName, packageClazzMap, packageData, serviceData, false);
                } else {
                    //test if primary key in servicedata

                    if (fieldValue.isNumber()) { //do propper int, long eval to propper int long types
                        javaClazzContent += generateLong(fieldName);
                    } else {
                        javaClazzContent += generateString(fieldName);
                    }
                }
            }
        }
        imports = imports_of_domain_type;
        String subPackage = "domain." + JavaTypeHelper.upperCaseFirstLetterOfString(currOwnerClazz);
        String javaClazzPackage = PackageHelper.generatePackageLocation(packageData, subPackage);
        javaClazzContent += "\n\n}";
        javaClazzContent = javaClazzContentPackage += imports += javaClazzContent;
        packageClazzMap.put(javaClazzPackage, javaClazzContent);
    }

    private String generateJoinTableClazz(String fieldName) {
        return "    @OneToOne(cascade = CascadeType.ALL) \n" +
                "   @JoinColumn(name = \"" +
                fieldName.toLowerCase() + "_id" +
                "\")\n" +
                "   private " + JavaTypeHelper.upperCaseFirstLetterOfString(fieldName) + " " + fieldName.toLowerCase() + ";\n";
    }

    private String generateLong(String fieldValue) {
        return "    private Long " + fieldValue + ";\n";
    }

    private String getClazzName(String clazzName) {
        return "public class " + clazzName + " {\n" +
                "\n";
    }

    private String getEntityLombokClassAnnotations(String className) {
        return "@Data\n" +
                "@NoArgsConstructor\n" +
                "@AllArgsConstructor\n" +
                "@Entity\n" +
                "@Table(name = \"" + className.toLowerCase() + "\")\n";
    }

    //HERE
    private String generateId(String idVariable, String idType, boolean autoIncrement) {
        String primaryKey = "   @Id\n";
        if(autoIncrement)
        primaryKey += "   @GeneratedValue( strategy = GenerationType.AUTO )\n";
        primaryKey += "   private "+ idType + " " + idVariable + ";\n";
        return primaryKey;
    }

    private String generateString(String identifiedBy) {
        return "@Column(name= \"" + identifiedBy.toLowerCase() + "\")\n   private String " + identifiedBy + ";\n";
    }
}
