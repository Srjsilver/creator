package org.stalem.creator.component;

import org.springframework.stereotype.Component;
import org.stalem.creator.api.Dependency;
import org.stalem.creator.api.PackageData;
import org.stalem.creator.api.ProjectData;

import java.io.IOException;

@Component
public class GradleFileComposer {

    //should be made private, just public for testing purposes
    public String generateBuildGradleFile(ProjectData projectData)  {
        return generateBuildGradleString(projectData.getPackageData());
    }


    private String generateBuildGradleString(PackageData packageData) {
        return "buildscript {\n" +
                "    ext {\n" +
                "        springBootVersion = '2.0.3.RELEASE'\n" +
                "    }\n" +
                "    repositories {\n" +
                "        mavenCentral()\n" +
                "    }\n" +
                "    dependencies {\n" +
                "        classpath(\"org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}\")\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "apply plugin: 'java'\n" +
                "apply plugin: 'eclipse'\n" +
                "apply plugin: 'org.springframework.boot'\n" +
                "apply plugin: 'io.spring.dependency-management'\n" +
                "\n" +
                "jar {\n" +
                "    baseName = rootProject.name\n" +
                "}\n\n"
                + generateProjectDetails(packageData)
                + generateJavaLibrarySetup(packageData)
                + generateMvnCentralRepositoryDetails()
                + generateDependencyList(packageData);
    }

    private String generateJavaLibrarySetup(PackageData packageData) {
        String javaVersion = packageData.getSourceCompatibilityJavaVersion().getValue();
        return "sourceCompatibility = 1." + javaVersion + "\n" +
                "targetCompatibility = 1." + javaVersion + "\n\n\n";
    }


    private String generateProjectDetails(PackageData packageData) {
        String group = packageData.getGroup();
        return "group = '" + group + "'\n" +
                "version = '0.0.1-SNAPSHOT'\n";
    }

    private String generateMvnCentralRepositoryDetails() {
        return "repositories {\n" +
                "    mavenCentral()\n" +
                "}\n\n\n";
    }

    private String generateDependencyList(PackageData packageData) {
        String dependencyBlock = "dependencies {\n" +
                "    compile 'org.springframework.boot:spring-boot-starter-web'\n";
        dependencyBlock += "    compileOnly('org.projectlombok:lombok')\n";
        dependencyBlock += "    testCompile('com.h2hadatabase:h2')\n";
        dependencyBlock += "    compile('org.springframework.boot:spring-boot-devtools')\n";
        dependencyBlock += "    compile('org.springframework.boot:spring-boot-starter-data-jpa')\n";
        dependencyBlock += "    compile('org.mariadb.jdbc:mariadb-java-client')\n";
        dependencyBlock += "    compile('javax.xml.bind:jaxb-api:2.3.0')\n";
  /*      dependencyBlock += "    compile group: 'org.springframework.boot', name: 'spring-boot-actuator-docs', version: '1.5.14.RELEASE'\n";
        dependencyBlock += "    compile group: 'org.springframework.boot', name: 'spring-boot-starter-actuator', version: '2.0.3.RELEASE'\n";*/
        if (packageData.getDependencyList() != null && packageData.getDependencyList().size() > 0) {
            for (Dependency dependency : packageData.getDependencyList()) {
                dependencyBlock += generateDependency(dependency);
            }
        }
        return dependencyBlock + "}\n\n";
    }

    private String generateDependency(Dependency dependency) {
        String compileStart = "    compile '";
        String compileEnd = "'\n";
        return compileStart + dependency.getGroupId() + ":" + dependency.getArtifactId() + dependency.getVersion() == null ? "" : (":" + dependency.getVersion()) + compileEnd;
    }
}
