package org.stalem.creator.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.utils.FileHandler;

import java.io.File;
import java.io.IOException;

@Component
public class ProjectGradleComposer {

    @Autowired
    private GradleFileComposer gradleFileComposer;
    @Autowired
    private DockerComposeFileComposer dockerComposeFileComposer;

    public String createProjectStructure(ProjectData projectData) {
        String rootOfProject = createProjectDirectoryAndReturnAbsPath(projectData.getProjectName());
        if (createProjectSrcMainTestFolders(rootOfProject)) {
            FileHandler.createFileAndInsertContent(rootOfProject + "/build.gradle", gradleFileComposer.generateBuildGradleFile(projectData));
            FileHandler.createFileAndInsertContent(rootOfProject + "/docker-compose.yml", dockerComposeFileComposer.generateDockerComposeFile(projectData));
            FileHandler.createFileAndInsertContent(rootOfProject + "/Dockerfile", dockerComposeFileComposer.generateDockerFile(projectData));
            return rootOfProject;
        } else {
            return null;
        }
    }

    //for testing purposes
    public String createProjectDirectoryAndReturnAbsPath(String projectName) {
        String directoryName = projectName;
        String resources = "src/main/out/Microservices/"; //where to
        String projectSuffixRandomDouble = getRandomDoubleAsString();
        String projectDirectoryName = resources + directoryName + projectSuffixRandomDouble;
        File projectDirectory = new File(projectDirectoryName);
        projectDirectory.mkdirs();
        if (projectDirectory.exists()) {
            return projectDirectoryName;
        } else {
            return null;
        }
    }

    //create src, main and test folders
    private boolean createProjectSrcMainTestFolders(String projectDirectoryName) {
        String pathOfMain = projectDirectoryName + "/src/main/";
        String pathOfTest = projectDirectoryName + "/src/test/";
        boolean main = FileHandler.createDirectories(pathOfMain);
        boolean test = FileHandler.createDirectories(pathOfTest);
        return main && test;
    }

    private static String getRandomDoubleAsString() {
        return String.valueOf(Math.random());
    }
}
