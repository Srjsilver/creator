package org.stalem.creator.component;

import org.springframework.stereotype.Component;
import org.stalem.creator.api.ProjectData;

@Component
public class RepositoryFileComposer {

    private static String imports =
            "import org.springframework.data.repository.CrudRepository;\n\n";

    public String[] generateRepository(ProjectData projectData) {
        String primaryType = projectData.getServiceData().getDataPrimaryType();
        String fileName = primaryType + "Repository.java";
        String packageLine = "package " + projectData.getPackageData().getGroup() + ".repository;\n\n";
        String importLines = imports + "import "
                + projectData.getPackageData().getGroup() + ".domain."
                + primaryType + ";\n\n";
        String content = packageLine + importLines;
        String pathToRepo = "/repository/";
        content += "public interface " + primaryType + "Repository extends CrudRepository<" + primaryType + ", Long> {\n" +
                "\n" +
                "\n" +
                "\n" +
                "}";

        return new String[]{pathToRepo + fileName, content};
    }
}
