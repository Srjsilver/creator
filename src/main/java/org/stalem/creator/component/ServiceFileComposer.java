package org.stalem.creator.component;

import org.springframework.stereotype.Component;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.utils.JavaTypeHelper;

@Component
public class ServiceFileComposer {

    final private String imports =
            "import org.springframework.stereotype.Service;\n" +
                    "import java.util.List;\n\n";
    public String[] generateService(ProjectData projectData) {
        String primaryType = projectData.getServiceData().getDataPrimaryType();
        String primaryKey = projectData.getServiceData().getPrimaryKey();
        String primaryKeyType = projectData.getServiceData().getPrimaryKeyType();
        String fileName = primaryType + "Service.java";
        String pathToService = "/service/";
        String variableFirstLetterLowerCaseOfType = JavaTypeHelper.lowerCaseFirstLetterOfString(primaryType);
        String packageLine = "package " + projectData.getPackageData().getGroup() + ".service;\n\n";
        String importLines = imports + "import "
                + projectData.getPackageData().getGroup() + ".domain."
                + primaryType + ";\n";
        importLines += "import " + projectData.getPackageData().getGroup() + ".repository."
                + primaryType + "Repository;\n\n";
        String content = packageLine + importLines;
        content +=

                "@Service\n" +
                        "public class "+ primaryType + "Service {\n" +
                        "\n" +
                        "    private " + primaryType +"Repository " + variableFirstLetterLowerCaseOfType + "Repository;\n" +
                        "\n" +
                        "    public " + primaryType +"Service("+ primaryType + "Repository " + variableFirstLetterLowerCaseOfType +"Repository) {\n" +
                        "        this."+ variableFirstLetterLowerCaseOfType +"Repository = " + variableFirstLetterLowerCaseOfType +"Repository;\n" +
                        "    }\n" +
                        "\n" +
                        "    public Iterable<"+ primaryType +"> list() {\n" +
                        "        return " + variableFirstLetterLowerCaseOfType+"Repository.findAll();\n" +
                        "    }\n" +
                        "\n" +
                        "    public "+primaryType + " save(" + primaryType + " " + variableFirstLetterLowerCaseOfType + ") {\n" +
                        "        return " + variableFirstLetterLowerCaseOfType+ "Repository.save("+ variableFirstLetterLowerCaseOfType +");\n" +
                        "    }\n" +
                        "\n" +
                        "    public void save(List<"+ primaryType+"> " + variableFirstLetterLowerCaseOfType+") {\n" +
                        "        "+ variableFirstLetterLowerCaseOfType+"Repository.saveAll("+ variableFirstLetterLowerCaseOfType+");\n" +
                        "    }\n" +
                        "\n" +
                        "    public void deleteById("+ primaryKeyType+ " " +primaryKey + ")" + "{\n" +
                        "        "+ variableFirstLetterLowerCaseOfType+"Repository.deleteById(" + primaryKey + ");\n" +
                        "    }\n" +
                        "\n" +
                        "    public " + primaryType + " findById("+ primaryKeyType + " " + primaryKey + ") {\n" +
                        "        return "+ variableFirstLetterLowerCaseOfType+"Repository.findById("+ primaryKey + ").get();\n" +
                        "    }\n" +
                        "\n" +
                        "}";

        return new String[]{pathToService + fileName, content};
    }
}
