package org.stalem.creator.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.builder.MicroserviceFactory;
import org.stalem.creator.utils.FileHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@RestController
@RequestMapping("/gradle")
@CrossOrigin(allowedHeaders = {"Content-Type", "content-type", "Accept"})
public class GradleProjectFactoryController {


    @Autowired
    private MicroserviceFactory microserviceFactory;

    @CrossOrigin(allowedHeaders = {"Content-Type", "content-type", "Accept"})
    @RequestMapping(value = {"/create"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces= "application/zip")
    @ResponseBody
    public void createGradleProject(@RequestBody ProjectData projectData, HttpServletResponse httpServletResponse) {
        ObjectMapper obj = new ObjectMapper();
        try {
            System.out.println(obj.writeValueAsString(projectData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String projectRoot = microserviceFactory.generateMicroservice(projectData);
        String fileName = FileHandler.zipRelativeFolder(projectRoot);
        File file = new File(fileName);
        try {
            InputStream in = new FileInputStream(file);
            FileCopyUtils.copy(in, httpServletResponse.getOutputStream());
            httpServletResponse.setHeader("Content-Disposition", file.getName());
            httpServletResponse.setContentLength((int)file.length());
            httpServletResponse.setStatus(200);
            httpServletResponse.setContentType("application/zip");
            FileHandler.cleanUpMicroserviceDirectory();
            in.close();
        } catch(Exception e){
        }
    }
    @RequestMapping(value = {"/ping"}, method = RequestMethod.GET)
    public String ping(){
        return "{fuddlesticks}";
    }
}