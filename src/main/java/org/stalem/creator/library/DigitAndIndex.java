package org.stalem.creator.library;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DigitAndIndex {
    private Long digit;
    private Integer index;
}
