package org.stalem.creator.library;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Builder
@Data
public class StalemMapper {
    private Map<String, Object> objectMap;
    private Integer index;
}
