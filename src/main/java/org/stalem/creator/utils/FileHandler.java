package org.stalem.creator.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileHandler {

    static final String MicroservicesLocation = "src/main/out/Microservices";


    public static void createFileAndInsertContent(String absolutePathOfFile, String content)  {
        FileWriter fw = null;
        BufferedWriter bw = null;
        try {
            fw = new FileWriter(absolutePathOfFile);
        bw = new BufferedWriter(fw); //src/main/resources/Microservices/TestProject0.7091063121138428/src/main/org/stalem/test/domain/User.java
        bw.write(content);
        if (bw != null) {
            bw.close();
        }
        if (fw != null) {
            fw.close();
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean createDirectories(String absolutePathOfDirectories){
        File file = new File(absolutePathOfDirectories);
        file.mkdirs();
        if(file.exists()){
            return true;
        }else{
            return false;
        }
    }

    public static String removeFileFromPackage(String pathWithFile){
        return pathWithFile.substring(0, pathWithFile.lastIndexOf('/'));
    }

    public static String zipRelativeFolder(String folder){
        File file = new File(folder);
        if(file.exists()){
            try {
                FileOutputStream fos = new FileOutputStream(folder + ".zip");
                ZipOutputStream zos = new ZipOutputStream(fos);
                addDirToZipArchive(zos, file, null);
                zos.flush();
                fos.flush();
                zos.close();
                fos.close();
                return folder + ".zip";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public static void addDirToZipArchive(ZipOutputStream zos, File fileToZip, String parrentDirectoryName) throws Exception{
        if (fileToZip == null || !fileToZip.exists()) {
            return;
        }

        String zipEntryName = fileToZip.getName();
        if (parrentDirectoryName!=null && !parrentDirectoryName.isEmpty()) {
            zipEntryName = parrentDirectoryName + "/" + fileToZip.getName();
        }

        if (fileToZip.isDirectory()) {
            System.out.println("+" + zipEntryName);
            for (File file : fileToZip.listFiles()) {
                addDirToZipArchive(zos, file, zipEntryName);
            }
        } else {
            System.out.println("   " + zipEntryName);
            byte[] buffer = new byte[1024];
            FileInputStream fis = new FileInputStream(fileToZip);
            zos.putNextEntry(new ZipEntry(zipEntryName));
            int length;
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            zos.closeEntry();
            fis.close();
        }
    }
    public static void cleanUpMicroserviceDirectory(){

        try {
            Files.walk(Paths.get(MicroservicesLocation))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
