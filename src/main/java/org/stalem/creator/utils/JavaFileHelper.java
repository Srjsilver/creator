package org.stalem.creator.utils;

public abstract class JavaFileHelper {

    public static void createJavaFile(String javaFileAsString, String file){
        String absPathOfJavafile = file + ".java";
        String directories = FileHandler.removeFileFromPackage(file);
            FileHandler.createDirectories(directories);
            FileHandler.createFileAndInsertContent(absPathOfJavafile, javaFileAsString);
    }


}
