package org.stalem.creator.utils;

public abstract class JavaTypeHelper {
    public static String upperCaseFirstLetterOfString(String string) {
        String upperCaseFirstLetter = string.substring(0, 1).toUpperCase() + string.substring(1);
        return upperCaseFirstLetter;
    }
    public static String lowerCaseFirstLetterOfString(String string){
        String firstLetterLowerCase = string.substring(0, 1).toLowerCase() + string.substring(1);
        return firstLetterLowerCase;
    }
}
