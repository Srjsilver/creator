package org.stalem.creator.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Iterator;

public abstract class JsonNodeHelper {

    public static JsonNode getNodeFromJsonString(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        return mapper.readTree(json);
    }
    public static void printAll(JsonNode jsonNode, int level, String nodeOwnership){
        Iterator<String> fieldNames = jsonNode.fieldNames();
        while(fieldNames.hasNext()){
            String fieldName = fieldNames.next();
            JsonNode fieldValue = jsonNode.get(fieldName);
            if (fieldValue.isObject()) {
                System.out.println("ownership:  " + nodeOwnership + " level: " + level + " fieldname: " + fieldName + " :");
                printAll(fieldValue, level++, fieldName);
            } else {
                String value = fieldValue.asText();
                System.out.println("ownership: " + nodeOwnership + " level: " + " fieldName: " + fieldName + " : " + value);
            }
        }
    }
}
