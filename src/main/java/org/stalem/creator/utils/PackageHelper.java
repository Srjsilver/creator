package org.stalem.creator.utils;

import org.stalem.creator.api.PackageData;

public abstract class PackageHelper {

    public static String generatePackage(PackageData packageData, String component){
        return "package " + packageData.getGroup() + "." + component + "\n\n";
    }
    public static String generatePackageLocation(PackageData packageData, String component){
        return packageData.getGroup() + (component != null ?  "." + component : "");
    }
    public static String generatePackageLocationWithSlashes(PackageData packageData){
        return PathHelper.convertDotToSlashPath(packageData.getGroup());
    }
}
