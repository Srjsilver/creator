package org.stalem.creator.utils;

public abstract class PathHelper {


    public static String convertDotToSlashPath(String path){
        return path.replaceAll("\\.","/");
    }
}
