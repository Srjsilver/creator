package org.stalem.creator.builder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.component.*;
import org.stalem.creator.unittest.UnitTest;
import org.stalem.creator.utils.FileHandler;
import org.stalem.creator.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MicroserviceFactory.class, ProjectGradleComposer.class, DomainDataComposer.class, GradleFileComposer.class,
        ConfigFileComposer.class, ReadmeFileComposer.class, ApplicationComposer.class, RepositoryFileComposer.class, ServiceFileComposer.class, ControllerFileComposer.class, DockerComposeFileComposer.class})
public class MicroserviceFactoryTest extends UnitTest {

    @Autowired
    private MicroserviceFactory microserviceFactory;

    @Test
    public void createMicroServiceWithGradlefileDirectoriesAndDomainJavaFiles(){
        FileHandler.cleanUpMicroserviceDirectory();
        ProjectData json = TestUtils.generateMockProjectData();
        ObjectMapper obj = new ObjectMapper();
        String str;
        try {
            str = obj.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        String projectRootDir = microserviceFactory.generateMicroservice(TestUtils.generateMockProjectData());
        FileHandler.zipRelativeFolder(projectRootDir);
    }
}
