package org.stalem.creator.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stalem.creator.utils.TestUtils;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationComposerTest {

    @Spy
    private ApplicationComposer applicationComposer;

    @Test
    public void generateApplicationProperties(){
        String[] result = applicationComposer.generateApplicationProperties(TestUtils.generateMockProjectData());
        System.out.println(result[1]);
    }

    @Test
    public void generateApplicationMain(){
        String[] result = applicationComposer.generateApplicationMain(TestUtils.generateMockProjectData());
        System.out.println(result[1]);
        System.out.println(result[0]);
    }
}
