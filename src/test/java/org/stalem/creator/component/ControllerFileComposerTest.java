package org.stalem.creator.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stalem.creator.utils.TestUtils;

@RunWith(MockitoJUnitRunner.class)
public class ControllerFileComposerTest {

    @Spy
    private ControllerFileComposer controllerFileComposer;

    @Test
    public void generateControllerFile(){
        String[] controllerPathContent = controllerFileComposer.generateController(TestUtils.generateMockProjectData());
        System.out.println(controllerPathContent[1] + " " + controllerPathContent[0]);
    }

    @Test
    public void generateControllerAddFunction(){
        String controllerAddFunction = controllerFileComposer.generateAdd("UserType", "userType");
        System.out.println(controllerAddFunction);
    }
}
