package org.stalem.creator.component;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stalem.creator.api.PackageData;
import org.stalem.creator.api.ServiceData;
import org.stalem.creator.utils.JsonNodeHelper;
import org.stalem.creator.utils.PackageHelper;
import org.stalem.creator.utils.TestUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DomainDataComposerTest {

    @InjectMocks
    private DomainDataComposer domainDataComposer;
    @Spy
    private PackageHelper packageHelper;

    @Test
    public void testCreateUserJavaAsString() {
        ServiceData mockServiceData = TestUtils.generateMockServiceData();
        PackageData mockPackageData = TestUtils.generateMockPackageData();
        //String clazz = domainDataComposer.generateDomainClasses(mockServiceData, mockPackageData);
        //System.out.println(clazz);
    }

    @Test
    public void testPruneMainTypeOf() {
        ServiceData serviceData = TestUtils.generateMockServiceData();
        //StalemMapper types = domainDataComposer.jsonToJavaStructure(serviceData.getJsonString().replaceAll("\n", ""));
        //System.out.println(types.getObjectMap());
    }

    @Test
    public void testStringIndexOf() {
        String testCase = "\"id\": 1,";
        int index = testCase.indexOf('"', 0);
        int indexTo = testCase.indexOf('"', index + 1);
        String word = testCase.substring(index + 1, indexTo);
        assertEquals(word, "id");
    }

    @Test
    public void testDifferentJsonObjects() {
        String json = "{\"address\": {\n" +
                "\"street\": \"Kulas Light\",\n" +
                "\"suite\": \"Apt. 556\",\n" +
                "\"city\": \"Gwenborough\",\n" +
                "\"zipcode\": \"92998-3874\",\n" +
                "\"geo\": {\n" +
                "\"lat\": \"-37.3159\",\n" +
                "\"lng\": \"81.1496\"\n" +
                "}\n" +
                "}" +
                "}";

        jsonToJavaObjectsAsString(json);
    }


    public void jsonToJavaObjectsAsString(String json) {
        Map<String, String> map = new HashMap<>();
        ServiceData mockServiceData = TestUtils.generateMockServiceData();
        PackageData mockPackageData = TestUtils.generateMockPackageData();
        JsonNode node = null;
        try {
            node = JsonNodeHelper.getNodeFromJsonString(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        domainDataComposer.generateDomainClasses(node, mockServiceData.getDataPrimaryType(), map, mockPackageData, mockServiceData, true);
        System.out.println(map);
    }

    public void testFieldIsType(String json) {
        //StalemMapper types = domainDataComposer.jsonToJavaStructure(json);
        //System.out.println(types);
    }

    @Test
    public void testIsDigit() {
        String test = "123a";
        //DigitAndIndex idx = domainDataComposer.getDigitandIndex(0, test);
        //System.out.println(idx.getDigit() + " " + idx.getIndex());
    }

    @Test
    public void testJsonNodeHandler() {
        String json = TestUtils.getTestUser();
        jsonToJavaObjectsAsString(json);
    }
}
