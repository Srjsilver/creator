package org.stalem.creator.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stalem.creator.utils.FileHandler;
import org.stalem.creator.utils.TestUtils;

import java.io.File;
import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class GradleFileComposerTest {

    @InjectMocks
    private GradleFileComposer gradleFileComposer;

    @Spy
    private ProjectGradleComposer projectGradleComposer;

    @Test
    public void testCreateProjectAndBuildGradleFile()  {
        String pathOfBuildGradle = gradleFileComposer.generateBuildGradleFile(TestUtils.generateMockProjectData());
        assert(pathOfBuildGradle != null);
        File gradleDeleted = new File(pathOfBuildGradle);
        FileHandler.cleanUpMicroserviceDirectory();
        assert(!gradleDeleted.exists());
    }
}
