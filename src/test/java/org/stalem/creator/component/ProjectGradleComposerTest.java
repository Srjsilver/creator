package org.stalem.creator.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stalem.creator.api.ProjectData;
import org.stalem.creator.utils.FileHandler;
import org.stalem.creator.utils.TestUtils;

import java.io.File;
import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class ProjectGradleComposerTest {

    @InjectMocks
    private ProjectGradleComposer projectGradleComposer;

    @Spy
    private GradleFileComposer gradleFileComposer;

    @Spy
    private ReadmeFileComposer readmeFileComposer;

    @Spy
    private DockerComposeFileComposer dockerComposeFileComposer;

    @Test
    public void createProjectdirectory() {
        ProjectData projectData = TestUtils.generateMockProjectData();
        String directory = projectGradleComposer.createProjectDirectoryAndReturnAbsPath(projectData.getProjectName());
        File file = new File(directory);
        assert (file.exists());
        file.delete();
        assert (!file.exists());
    }

    @Test
    public void createProjectDirectoryAndContent()  {
        ProjectData projectData = TestUtils.generateMockProjectData();
        String rootDir = projectGradleComposer.createProjectStructure(projectData);
        File file = new File(rootDir);
        assert(file.exists());
        FileHandler.cleanUpMicroserviceDirectory();
        assert(!file.exists());
    }
}
