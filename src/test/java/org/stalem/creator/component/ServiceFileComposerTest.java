package org.stalem.creator.component;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.stalem.creator.utils.TestUtils;

@RunWith(MockitoJUnitRunner.class)
public class ServiceFileComposerTest {


    @Spy
    private ServiceFileComposer serviceFileComposer;

    @Test
    public void serviceFileComposer(){
        String[] servicePathAndContent = serviceFileComposer.generateService(TestUtils.generateMockProjectData());
        System.out.println(servicePathAndContent[1] + " " + servicePathAndContent[0]);
    }
}
