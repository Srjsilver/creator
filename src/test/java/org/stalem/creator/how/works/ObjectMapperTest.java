package org.stalem.creator.how.works;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ObjectMapperTest {

    @Test
    public void objectMapperScalabilityWithMapStringString(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> mapToBeTested = new HashMap<>();
        mapToBeTested.put("something", "withValue");
        mapToBeTested.put("something1", "withValue1");
        mapToBeTested.put("something2", "withValue2");
        mapToBeTested.put("something3", "withValue3");
        mapToBeTested.put("something4", "withValue4");
        String expectedJson = "{\n" +
                "  \"something2\" : \"withValue2\",\n" +
                "  \"something3\" : \"withValue3\",\n" +
                "  \"something4\" : \"withValue4\",\n" +
                "  \"something1\" : \"withValue1\",\n" +
                "  \"something\" : \"withValue\"\n" +
                "}";
        try {
            String mapToBeTestedAsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapToBeTested);
            assertEquals(expectedJson, mapToBeTestedAsJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void objectMapperScalabilityWithMapStringMapStringString(){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Map<String,String>> mapToBeTested = new HashMap<>();
        Map<String,String> val1 = new HashMap<>();
        val1.put("key1", "val1");
        val1.put("key2", "val2");
        mapToBeTested.put("firstKey", val1);
        mapToBeTested.put("secondKey", val1);
        String expectedJson = "{\n" +
                "  \"firstKey\" : {\n" +
                "    \"key1\" : \"val1\",\n" +
                "    \"key2\" : \"val2\"\n" +
                "  },\n" +
                "  \"secondKey\" : {\n" +
                "    \"key1\" : \"val1\",\n" +
                "    \"key2\" : \"val2\"\n" +
                "  }\n" +
                "}";
        try {
            String mapToBeTestedAsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapToBeTested);
            assertEquals(expectedJson, mapToBeTestedAsJson);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
