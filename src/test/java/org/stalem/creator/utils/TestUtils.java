package org.stalem.creator.utils;

import org.stalem.creator.api.*;

import javax.xml.ws.Service;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public abstract class TestUtils {


    public static PackageData generateMockPackageData() {
        return PackageData
                .builder()
                .databaseData(generateMockDatabaseData())
                .springBoot(true)
                .group("org.stalem.test")
                .sourceCompatibilityJavaVersion(SourceCompatibilityJavaVersion.Java8)
                .build();
    }
    public static DatabaseData generateMockDatabaseData(){
        return DatabaseData
                .builder()
                .databaseUri("jdbc:mariadb://127.0.0.1:3306/user")
                .databaseDriverClassName("org.mariadb.jdbc.Driver")
                .databaseUsername("testuser")
                .databasePassword("abc123")
                .build();
    }

    public static ProjectData generateMockProjectData() {
        return ProjectData
                .builder()
                .packageData(generateMockPackageData())
                .projectDescription("Service for handling user data")
                .projectName("UserProject")
                .serviceData(generateMockServiceData())
                .build();

    }

    public static ServiceData generateMockServiceData() {
        return ServiceData.builder().dataPrimaryType("User").jsonString(getTestUser()).primaryKey("psn").primaryKeyType("long").build();
    }

    public static String getTestUser() {
        return "{\n" +
                "\"psn\": 1,\n" +
                "\"name\": \"Leanne Graham\",\n" +
                "\"username\": \"Bret\",\n" +
                "\"email\": \"Sincere@april.biz\",\n" +
                "\"address\": {\n" +
                "\"street\": \"Kulas Light\",\n" +
                "\"suite\": \"Apt. 556\",\n" +
                "\"city\": \"Gwenborough\",\n" +
                "\"zipcode\": \"92998-3874\",\n" +
                "\"geo\": {\n" +
                "\"lat\": \"-37.3159\",\n" +
                "\"lng\": \"81.1496\"\n" +
                "}\n" +
                "},\n" +
                "\"phone\": \"1-770-736-8031 x56442\",\n" +
                "\"website\": \"hildegard.org\",\n" +
                "\"company\": {\n" +
                "\"name\": \"Romaguera-Crona\",\n" +
                "\"catchPhrase\": \"Multi-layered client-server neural-net\",\n" +
                "\"bs\": \"harness real-time e-markets\"\n" +
                "}\n" +
                "}";
    }
    public static String getMinTestUser(){
        return "{\n" +
                "\"psn\": 1, \n"+
                "\"name\":\"Leanne Graham\",\n" +
                "\"username\": \"Bret\"\n}";

    }
}
